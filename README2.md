
# Unit 1 Day 2 Activity

## Instructions

### Step 1. Update the styling for "Our Services"

Inspect your HTML document and find the element with class "card" that is associated with the card containing the "Our Services" header.

Let's say you've been tasked with updating the "Our Services" card because, as a company, we really want to highlight and make visible to the user what services we provide.

In order to do this, you've been asked to change the background of the card to a shade of red and underline each list item.

Utilize the `Styles` pane of the Dev tools in order to experiment with each request. Once you have accomplished your task, update your `styles.css` file within your root directory so your changes persist after reload.

### Step 2. Updating HTML Elements

make our web site interactive, it is not extraordinarily functional.

Currently our "About us" section only has placeholder text.

1. Use the dev tools to identify the HTML tags that are related to the placeholder text:
   `Lorem ipsum dolor sit amet, consectetur adipiscing elit.Suspendisse aliquet urna eu quam placerat, eget euismod justo gravida`
1. Double click on the text within your DOM tree and update it to whatever you'd like (keep it professional!)
2. Update the corresponding HTML element within your VS Code to ensure your changes will persist after reload.

Now whoever visits our webpage can know what we our company is really about!

## Final Task

Create a 3 minute video discussing how html, CSS, and Javascript in the browser works. You should demonstrate:

1. Manipulating HTML in the inspector tab. After showing the changes that occur in the browser. Make the same changes within your VS Code to ensure they persist.
2. Updating styles in the styles pane. After showing the changes that occur in the browser. Make the same changes within your VS Code to ensure they persist.
3. Show where the JS is executed in the Browser and correlate it with your VS Code File. After demonstrating this, update the JS code to make whatever change you'd like and show the effect within the browser.
