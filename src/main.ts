
// Get the header element
const headerElement: HTMLHeadingElement | null = document.querySelector('header h1');

// Get the button element
const changeHeaderTextButton: HTMLButtonElement | null = document.getElementById('changeHeaderText') as HTMLButtonElement | null;

// Define the action to be executed on click
const changeHeaderTextAction = (): void => {
  if (headerElement) {
    headerElement.textContent = "It's interactive!";
  }
};

// Add the click event listener to the button
if (changeHeaderTextButton) {
  changeHeaderTextButton.addEventListener('click', changeHeaderTextAction);
}
