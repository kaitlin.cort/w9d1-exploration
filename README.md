# Unit 1 Day 1 Activity

## Instructions

## Activity Time - Diagramming a Website

### Step 1: Create a diagram with screenshots which illustrates the sequence of events of a page loading from initial request to user interaction

1. After you've typed out your URL and pressed enter, your browser makes a request to retrieve the HTML file from the corresponding server.

   - [ ] Draw an arrow between URL, where the initial request is made to the corresponding HTML file within the network tab.
   - [ ] Finally, connect this to the HTML file within your VS Code.

2. Any additional files that are needed will be requested depending on their existence within the HTML file.

   - [ ] Draw an arrow for each of the specific tags in your DOM tree and the respective CSS and TS files within VS Code that each tag is associated with.

3. After all files have been requested, the DOM tree begins rendering.

   - [ ] Draw an arrow between an HTML element within your VS Code and the corresponding node within the inspector pane of your Dev Tools
   - [ ] Finally, draw where that tag is being rendered on the web page

4. After the DOM tree begins rendering, associated styles will be applied to their corresponding classes.

   - [ ] Draw an arrow between a CSS class within your VS Code the corresponding style in the style pane of your dev tools
   - [ ] Finally, draw an arrow from the style within the style pane to the CSS style where it is rendered in the browser.
     - Hint: If you hover over the HTML node associated with the style, it will highlight the associated style, making it easy to visualize
   - Depending on the location of the script tag, JS/TS functionality will begin to execute after it has been parsed. In this case, our script tag is at the bottom and begins executing after HTML has been rendered.

### Step 2: Create a short video explaining how a web page loads from the initial request until the user interacts with the page. Use the Firefox dev tools and the provided application to demo this process. Reference the diagrams you created in the previous step when making your video
